React Boilerplate

Minimal boilerplate code to start with react app.

[![pipeline status](https://gitlab.com/salilkapoor/react-boilerplate/badges/master/pipeline.svg)](https://gitlab.com/salilkapoor/react-boilerplate/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/078aa086-52b6-443e-b624-79e1de2b9834/deploy-status)](https://app.netlify.com/sites/frosty-raman-0f9e16/deploys)

![Branch Coverage](./badges/badge-branches.svg)
![Functions Coverage](./badges/badge-functions.svg)
![Lines Coverage](./badges/badge-lines.svg)
![Statements Coverage](./badges/badge-statements.svg)

